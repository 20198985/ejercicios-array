using System;

namespace Ejercicio7
{
    class Program
    {
         static void Main(string[] args)
        {
            Console.WriteLine("Escribir nombre: ");
            string saludo = Console.ReadLine();
            int longitud = saludo.Length;
       
            for (int i = 0; i < longitud; i++)
            {
                Console.Write(" {1}", i, saludo[i]);
            }
            Console.ReadKey();
        }
    }
}
